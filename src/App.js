import React, { Component } from 'react';
import { MenuBar, Route } from './modules';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import Token from './lib/request/AuthToken';
import { AuthAPI } from './modules/auth/ResourceAPI';

import createHistory from 'history/createBrowserHistory';

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerReducer, routerMiddleware, } from 'react-router-redux';

import appReducer from './modules/Reducer';
import authReducer from './modules/auth/Reducer';


import './App.css';

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
  combineReducers({
    appReducer,
    authReducer,
    router: routerReducer,
  }),
  applyMiddleware(middleware),
);

class App extends Component {

  state = {
    has_token: false
  }

  componentWillMount = () => {
    if (Token.loadToken()) {
      AuthAPI.getCurrentUser(store.dispatch);
      this.setState({ has_token: true });
    }
  }

  componentWillUpdate = () => {
    if (!Token.loadToken()) {
      this.setState({ has_token: false });
    } else {
      this.setState({ has_token: true });
    }
  }

  render = () => {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <MuiThemeProvider theme={theme}>
            <MenuBar />
            <div style={{marginTop: '100px'}} >
              <Route has_token={this.state.has_token} />
            </div>
          </MuiThemeProvider>
        </ConnectedRouter>
      </Provider>
    );
  }

}

export default App;
