import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import FormsySelect from './FormsySelect';

class GridSelectField extends Component {

  static propTypes = {
    xs: PropTypes.number,
    field: PropTypes.object.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    defaultValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    options: PropTypes.array.isRequired,
  }

  static defaultProps = {
    defaultValue: 0,
    xs: 6,
  }

  render = () => {
    const {options, xs, field, value, defaultValue} = this.props;

    return (
      <Grid item xs={xs} >
        <FormsySelect
          fullWidth
          style={{width: '100%'}}
          value={value || defaultValue}
          {...field}
        >
          {options}
        </FormsySelect>
      </Grid>
    );
  }
}

export default GridSelectField;
