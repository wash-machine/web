import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

class FormsySelect extends Component {

  state = {
    value: 0
  }

  handleChange = (event) => {
    const { setValue } = this.props;
    const value = event.target.value;
    setValue(value);
    this.setState({value});
  }

  render = () => {
    const { getValue, showError, getErrorMessage } = this.props;
    return (
      <FormControl fullWidth={this.props.fullWidth} error={showError()} >
        <InputLabel htmlFor={`select-${this.props.name}`}>{this.props.label}</InputLabel>
        <Select
          displayEmpty
          value={getValue()}
          onChange={this.handleChange}
          input={<Input name={this.props.name} id={`select-${this.props.name}`} />}
        >
          {this.props.children}
        </Select>
        <FormHelperText>{showError() ? getErrorMessage() : this.props.helperText}</FormHelperText>
      </FormControl>
    );
  }
}

export default withFormsy(FormsySelect);
