import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import FormsyText from './FormsyText';


class GridTextField extends Component {

  static propTypes = {
    xs: PropTypes.number,
    field: PropTypes.object.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    defaultValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  }

  static defaultProps = {
    defaultValue: undefined,
    xs: 6,
  }

  render = () => {
    const {xs, field, value, defaultValue} = this.props;

    return (
      <Grid item xs={xs} >
        <FormsyText
          fullWidth
          {...field}
          value={value || defaultValue}
        />
      </Grid>
    );
  }
}

export default GridTextField;
