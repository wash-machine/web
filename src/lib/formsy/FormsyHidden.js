import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';

class FormsyHidden extends Component {
  render = () => {
    const value = this.props.getValue();
    const name = this.props.name;
    return (<input type='hidden' value={value} name={name} />);
  }
}

export default withFormsy(FormsyHidden);
