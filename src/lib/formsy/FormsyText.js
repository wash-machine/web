import React, { Component } from 'react';
import { withFormsy } from 'formsy-react';
import TextField from '@material-ui/core/TextField';

class FormsyText extends Component {

  static defaultProps = {
    underlineFocusStyle: {},
    underlineStyle: {},
    validationColor: '#4CAF50',
  }

  handleChange = (event) => {
    // Update the value (and so display any error) after a timeout.
    const { setValue } = this.props;

    // If there was an error (on loss of focus) update on each keypress to resolve same.
    const value = event.target.value;

    setValue(value);
  }

  render = () => {
    const {
      errorText,
      getErrorMessage,
      getErrorMessages,
      getValue,
      value,
      required,
      hasValue,
      hintText,
      isValid,
      isValidValue,
      isPristine,
      isRequired,
      isFormDisabled,
      isFormSubmitted,
      label,
      resetValue,
      setValidations,
      setValue,
      showError,
      showRequired,
      validations, // eslint-disable-line no-unused-vars
      validationColor,
      validationError, // eslint-disable-line no-unused-vars
      validationErrors, // eslint-disable-line no-unused-vars
      underlineStyle,
      underlineFocusStyle,
      ...rest } = this.props;


    return (
      <TextField
        error={showError()}
        required={showRequired()}
        label={label}
        disabled={isFormDisabled()}
        onChange={this.handleChange}
        value={getValue() || ''}
        {...rest}
        helperText={getErrorMessage()}
      />
    );
  }
}

export default withFormsy(FormsyText);
