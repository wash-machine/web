import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoadingMaterial from './LoadingMaterial';

class LoadingBase extends Component {

  static propTypes = {
    fetching: PropTypes.bool,
    previousFetch: PropTypes.bool,
  }

  render = () => {
    const { fetching, previousFetch } = this.props;
    // console.debug('Fetching', fetching, 'Previous', previousFetch);
    if (fetching && previousFetch) {
      return (
        <div>
          <LoadingMaterial linear />
          {this.props.children}
        </div>
      );
    } else if (fetching) {
      return <LoadingMaterial />;
    } else {
      return this.props.children;
    }
  }
}

export default LoadingBase;
