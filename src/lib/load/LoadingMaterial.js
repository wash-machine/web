import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  loading: {
    display: 'block',
    margin: 'auto',
  }
};

class Loading extends Component {

  static defaultProps = {
    linear: false,
  }

  render = () => {
    const { linear, classes } = this.props;
    if (linear) {
      return (
        <LinearProgress />
      );
    } else {
      return (
        <CircularProgress className={ classes.loading } size={ 50 } />
      );
    }
  }
}

export default withStyles(styles)(Loading);
