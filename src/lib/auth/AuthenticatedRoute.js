import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class NotAuthorizedRoute extends Component {
  render = () => {
    return <div>401</div>
  }
}

class AuthorizedRoute extends Component{

  static propTypes = {
    user: PropTypes.object,
    auth: PropTypes.bool,
    location: PropTypes.object,
  }

  render() {

    const user = this.props.user || {};
    const auth = this.props.auth;
    console.log(user.is_superuser, auth, user);

    if (user.is_superuser && auth) {
      return <Route {...this.props} />;
    } else if (!user.is_superuser && auth) {
      return <NotAuthorizedRoute />;
    } else if (this.props.has_token) {
      return null;
    }
    return <Redirect to={{
      pathname: '/login',
      state: { from: this.props.location }
    }} />;
  }
}

export default connect( (state) => ({
  user: state.authReducer.user,
  auth: state.authReducer.isAuthenticated,
}))(AuthorizedRoute);
