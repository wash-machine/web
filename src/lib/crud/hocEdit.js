import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import LoadingBase from '../../lib/load/LoadingBase';
// import { matchPath } from 'react-router'
// import { createMatchSelector } from 'react-router-redux';

const hocEdit = (FormComponent, loadItem, name) => {
  return class ItemEdit extends Component {

    static propTypes = {
      dispatch: PropTypes.func,
      location: PropTypes.object,
      items: PropTypes.array,
    }

    static defaultProps = {
      items: []
    }

    state = {
      item: {}
    }

    componentWillMount = () => {
      const { match } = this.props;
      this.getItem(match.params.id);
    }

    componentWillReceiveProps = (nextProps) => {
      const { match } = nextProps;

      // Try to get the params id only if it isn't null
      if (match && match.params && match.params.id !== this.state.item.id) {
        this.getItem(match.params.id);
      }

    }

    getItem = (index) => {
      if (index) {
        const indexNumber = Number(index);
        const item = this.props.items.find( element => element.id === indexNumber );
        if (item) {
          this.setState({item});
        } else {
          loadItem(this.props.dispatch, indexNumber);
        }
      }
    }

    render = () => {
      const item = {[name]: this.state.item};
      const { dispatch, location, items, fetching, ...rest } = this.props;
      return (
        <FormComponent {...item} {...rest} />
      );
    }
  };
};

export default hocEdit;
