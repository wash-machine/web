import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Form from './Form';
import { withRouter, Redirect } from 'react-router'

const CancelButton = withRouter( ({ history }) => {
  return (
    <Button onClick={() => history.goBack()}>
      Back
    </Button>
  );

});
class CardForm extends Component {

  static propTypes = {
    title: PropTypes.string,
  }

  state = {
    redirect: false,
  }

  componentWillReceiveProps = (nextProps) => {
    if (!this.props.success && nextProps.success) {
      this.setState({ redirect: true });
    }
  }

  render = () => {
    const { title, children, successUrl, ...rest } = this.props;
    if (!this.state.redirect) {
      return (
        <Form {...rest}>
          <Card>
            <CardHeader
              title={title}
            />
            <CardContent>
              {children}
            </CardContent>
            <CardActions>
              <CancelButton />
              <Button type='submit'>
                Save
              </Button>
            </CardActions>
          </Card>
        </Form>
      );
    } else {
      return (<Redirect to={successUrl} />);
    }
  }
}

export default CardForm;
