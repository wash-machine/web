import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Grid from '@material-ui/core/Grid';
import LoadingBase from '../load/LoadingBase';

const hocList = (ItemComponent) => {
  return class ModelList extends Component {

    state = {
      dataList: []
    }

    static defaultProps = {
      dataList: []
    }

    static propTypes = {
      dataList: PropTypes.array,
      fetching: PropTypes.bool,
    }

    componentWillMount = () => {
      this.setState({dataList: this.props.dataList});
    }

    componentWillReceiveProps = (nextProps) => {
      if (!_.isEqual(nextProps.dataList, this.props.dataList)) {
        this.setState({dataList: nextProps.dataList});
      }
    }

    render = () => {
      const { dataList, fetching, ...rest } = this.props;
      console.debug('hocList data list has length ', dataList.length);
      return (
        <LoadingBase
          fetching={ fetching }
          previousFetch={ dataList.length !== 0 }
        >
          <Grid container spacing={40}>
            {
              this.state.dataList.map( (dataItem, idx) => {
                return (
                  <Grid key={idx} item xs={4}>
                    <ItemComponent item={dataItem} {...rest} />
                  </Grid>
                )
              })
            }
          </Grid>
        </LoadingBase>
      );
    }
  };
};

export default hocList;
