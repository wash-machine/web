import React, { Component } from 'react';
import Formsy from 'formsy-react';
import _ from 'lodash';

class Form extends Component {

  state = {
    data: {}
  }

  submitData = (data) => {
    this.setState({data});
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.error) {
      const errors = _.reduce(nextProps.error,
        (prev, value, key) => {
          console.log(value);
          prev[key] = value.join(', ');
          return prev;
        }, {});
      this.form.updateInputsWithError(errors);
    }
  }

  render = () => {
    const { onSubmit } = this.props;
    const submitData = onSubmit ? onSubmit : this.submitData;
    return (
      <Formsy
        onValidSubmit={submitData}
        ref={(form) => this.form = form}
      >
        {this.props.children}
      </Formsy>
    );
  }
}

export default Form;
