import Token from './AuthToken';

const requestInfo = (method, body) => {
  const token = Token.loadToken();
  return {
    method: method,
    headers: new Headers({
      'Content-type': 'application/json',
      Authorization: Boolean(token) ? `Token ${token}`: '',
    }),
    body: Boolean(body) ? body: undefined,
  };
};

export const request = (url, method='GET', body='') => {
  const promise = new Promise( (resolve, reject) => {
    fetch(url, requestInfo(method, body))
      .then(response => {
        if (response.status >= 200 && response.status < 300) {
          resolve(response.json());
        } else if (response.status === 400) {
          reject({formError: response.json()});
        } else if (response.status === 401) {
          Token.removeToken();
          alert('Sorry, some problem with authentication, please login again!');
          window.location = '/login'
          reject(response.json());
        } else {
          reject(response.json());
        }
      })
      .catch((e) => reject(e));
  });

  return promise;
};

export const requestForm = (url, body) => {
  return new Promise( (resolve, reject) => {
    const method = body.id ? 'PUT':'POST';
    const newUrl = body.id ? `${url}${body.id}/`:url;
    request(newUrl, method, JSON.stringify(body)).then( data => {
        resolve(data);
    }).catch( data => {
      if(data && data.formError) {
        // The form error is a promise of response.json()
        data.formError.then(data => reject(data));
      }
    });
  });
};
