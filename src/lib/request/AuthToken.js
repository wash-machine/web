export default class Token {

  static loadToken = () => {
    const token = window.localStorage.token;
    if (token && token !== 'undefined' && token !== 'null') {
      return token;
    }
    return undefined;
  }

  static saveToken = (token) => {
    window.localStorage.token = token;
  }

  static removeToken = () => {
    delete window.localStorage.token;
  }

}
