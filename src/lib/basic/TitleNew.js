import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import theme from '../../theme';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const styles = {
  title: {
    marginBottom: `${theme.spacing.unit * 3}px`,
  }
};

class TitleNew extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    newLink: PropTypes.string.isRequired,
  }
  render = () => {
    const { classes, title, newLink } = this.props;
    return (
      <Grid container spacing={0}>
        <Grid item xs={10} className={classes.title} >
          <Typography variant='headline' >{title}</Typography>
        </Grid>
        <Grid item xs={2}>
          <Button
            component={(props) => <Link to={newLink} {...props} />}
            variant='raised'
            color='primary'
          >
            New
          </Button>
        </Grid>
      </Grid>
    );
  }
}
export default withStyles(styles)(TitleNew);
