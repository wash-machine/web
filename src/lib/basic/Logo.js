import React, { Component } from 'react';
import logo from '../../logo.png';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  image: {
    width: '400px',
  }
};

class Logo extends Component {

  render = () => {
    return (
      <img className={ this.props.classes.image } src={logo} />
    );
  }

}

export default withStyles(styles)(Logo);
