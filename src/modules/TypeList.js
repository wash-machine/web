import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

const styles = {
  show: {
    backgroundColor: '#f1f',
  }
};

class TypeList extends Component {

  static propTypes = {
    types: PropTypes.array,
    title: PropTypes.string,
    name: PropTypes.string,
    search: PropTypes.string,
  }

  static defaultProps = { types: [] }

  componentDidMount = () => {
    const item = document.querySelector(this.props.location.hash || '#none');
    if (item) {
      console.log('scroll into element', this.props.location.hash);
      item.scrollIntoView()
    }
  }

  render = () => {
    const {
      title, types, name, classes,
      location: {hash, search}
    } = this.props;
    const matchHash = hash.indexOf(name) !== -1;
    const id = Number((search.match(/id=(\d+)/) || ['', '-1'])[1]);

    if (types.length) {
      return (
        <Grid item xs={6} >
          <h3 id={`${name}`} >{title}</h3>
          {
            types.map( (type, key) => {
              const className = (matchHash && id === type.id) ? classes.show : '';
              return (
                <p key={key} className={ className } >
                  <label>{type.id} - {type.name}</label>
                </p>
              );
            })
          }
        </Grid>
      );
    } else {
      return (
        <Grid item xs={6} >
          <h3>{title}</h3>
          <p>No itens</p>
        </Grid>
      );
    }
  }
}

export default withStyles(styles)(TypeList);
