import React, { Component } from 'react';
import CardForm from '../../lib/crud/CardForm';
import PropTypes from 'prop-types';
//import {FormsyTextField} from '../form/formsy-fields';
import GridTextField from '../../lib/formsy/GridTextField';
import FormsyHidden from '../../lib/formsy/FormsyHidden';
import { connect } from 'react-redux';
import { MachineAPI } from './ResourceAPI';

class MachineForm extends Component {

  static propTypes = {
    item: PropTypes.object,
    title: PropTypes.string,
  }

  onSubmit = (data) => {
    MachineAPI.saveMachine(this.props.dispatch, data);
  }

  render = () => {
    const item = this.props.item || {};
    return (
      <CardForm
        title={this.props.title || 'New Machine'}
        onSubmit={this.onSubmit}
        successUrl={'/machines/'}
        success={this.props.success}
      >
        <FormsyHidden
          name='id'
          label='id'
          value={item.id || ''}
        />
        <FormsyHidden
          name='is_busy'
          label='Is busy'
          placeholder='Is busy'
          value={item.is_busy || false}
        />
        <GridTextField
          field={{name: 'name', label: 'Machine name'}}
          value={item.name || ''}
          xs={12}
        />
      </CardForm>
    );
  }
}

export default connect( state => ({
  success: state.appReducer.success,
  error: state.appReducer.machinesError,
})
)(MachineForm);
