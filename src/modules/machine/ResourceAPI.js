'use-strict';
import {receiveMachines, fetchMachines, receiveMachine, fetchMachine} from './Actions';
import { request, requestForm } from '../../lib/request';

const fakeData = [
  {
    id: 10,
    watter_flow: 10,
    errors: '',
    state: {name: 'available', code: 0}
  },{
    id: 11,
    watter_flow: 7,
    errors: 'xablau',
    state: {name: 'error', code: 9}
  },{
    id: 12,
    watter_flow: 11,
    state: {name: 'rub', code: 2}
  },{
    id: 13,
    watter_flow: 5,
    state: {name: 'dry', code: 1}
  },{
    id: 14,
    watter_flow: 8,
    state: {name: 'dry', code: 5}
  },{
    id: 15,
    watter_flow: 11,
    state: {name: 'dry', code: 6}
  }
];

const MachineAPI = {
  loadMachines: (dispatch) => {
    dispatch(fetchMachines()); // Notify get

    // Perform get
    return request("https://shoe-washer.herokuapp.com/api/service_order/shoe_washer/")
      .then( data => dispatch(receiveMachines(data)));
  },
  loadMachine: (dispatch, id) => {
    dispatch(fetchMachine());
    return new Promise((resolve, reject) => {
      setTimeout( (id) => {
        // console.debug('set timout fetch single', id);
        const machine = fakeData.find( i => i.id === id);
        // console.debug('machine of fake data', machine);
        if (machine) resolve(machine);
        else reject('Not fetch');
    }, 1000, id);
    }).then( data => {/*console.debug('data from fetch', data);*/ dispatch(receiveMachine(data)); });
  },
  saveMachine: (dispatch, data) => {
    requestForm('https://shoe-washer.herokuapp.com/api/service_order/shoe_washer/', data).
      then( (machine) => {
        dispatch(receiveMachine(machine));
      });
  }
};

export {MachineAPI};
