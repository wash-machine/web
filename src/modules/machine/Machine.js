import React, { Component } from 'react';

import MachineList from './MachineList';
import { connect } from 'react-redux';
import { MachineAPI } from './ResourceAPI';
import Grid from '@material-ui/core/Grid';
import TitleNew from '../../lib/basic/TitleNew';



class Machine extends Component {

  componentDidMount = () => {
    MachineAPI.loadMachines(this.props.dispatch);
  }

  render = () => {
    // console.log(this.props.machines);
    const { fetching, machines } = this.props;
    return (
      <Grid container spacing={0}>
        <TitleNew title='Machines' newLink='/machines/new' />
        <Grid item xs={12}>
            <MachineList fetching={ fetching } dataList={ machines } />
        </Grid>
      </Grid>
    );
  }
}

export default connect(state => ({
  machines: state.appReducer.machines,
  fetching: state.appReducer.fetching
}))(Machine);
