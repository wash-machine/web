import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Collapse from '@material-ui/core/Collapse';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
// import Opacity from '@@material-ui/core/icons/Opacity';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import theme from '../../theme';
import { Link } from 'react-router-dom';

const styles = {
  avatarOk: {
    color: theme.palette.primary.main,
    backgroundColor: theme.palette.primary.main,
  },
  avatarAvailable: {
    color: '#35b016',
    backgroundColor: '#35b016',
  },
  avatarError: {
    color: theme.palette.error.main,
    backgroundColor: theme.palette.error.main,
  },
  item: {
    paddingLeft: 0,
    paddingRight: 0
  },
  cardError: {
    borderColor: theme.palette.error.main,
    borderWidth: '1px',
    borderStyle: 'dotted',
  },
  cardContentError: {
  },
  iconError: {
    fontSize: '30px',
    marginRight: '16px',
    verticalAlign: 'middle',
  }
};

class MachineListItem extends Component {

  static propTypes = {
    item: PropTypes.object,
    onEdit: PropTypes.func,
  }

  state = {
    expanded: false,
  }

  getError = (error) => {
    return (
      <div>
        [{error.code}] - {error.name}<br />
      </div>
    );
  }

  handleExpand = () => {
    this.setState( (prevState) => {
      return {expanded: !prevState.expanded};
    });
  }

  render = () => {
    const {item, classes: {avatarOk, avatarError, avatarAvailable} } = this.props;
    const hasError = Boolean(item.errors.length);

    let classAvatar = '';
    if (hasError) {
      classAvatar = avatarError;
    } else if (item.is_busy) {
      classAvatar = avatarOk;
    } else {
      classAvatar = avatarAvailable;
    }

    return (
      <Card className={hasError ? this.props.classes.cardError : ''} >
        <CardHeader
          avatar={
            <Avatar className={classAvatar}>
            .
            </Avatar>
          }
          title={`${item.id} - ${item.name}`}
          subheader={`busy: ${item.is_busy}`}
        />
        {hasError &&
          <div>
            <CardActions disableActionSpacing>
              This machine has problems!
              <IconButton
                onClick={this.handleExpand}
                aria-expanded={this.state.expanded}
                aria-label="Show more"
              >
                <Icon>
                  expand_more
                </Icon>
              </IconButton>
            </CardActions>
            <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
              <CardContent>
                {item.errors.map(this.getError)}
              </CardContent>
            </Collapse>
          </div>
        }
      </Card>
    );
  }
}

export default withStyles(styles)(MachineListItem);
