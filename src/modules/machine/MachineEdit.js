import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MachineForm from './MachineForm';
import { MachineAPI } from './ResourceAPI';
import hocEdit from '../../lib/crud/hocEdit';
// import { matchPath } from 'react-router'
// import { createMatchSelector } from 'react-router-redux';

class MachineEdit extends Component {

  static defaultProps = {
    machine: {}
  }

  static propTypes = {
    machine: PropTypes.object
  }

  render = () => {
    console.log('hiehie\n\n\n', this.props);
    return (
      <MachineForm
        title={'Edit machine'}
        item={this.props.machine} />
    );
  }
}

export default connect( state => ({
  items: state.appReducer.machines,
  fetching: state.appReducer.fetching,
})
)(hocEdit(MachineEdit, MachineAPI.loadMachine, 'machine'));
