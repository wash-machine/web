import {fetchData, receiveData, fetchAllData, receiveAllData} from '../Actions';

const fetchMachines = () => ({
  ...fetchAllData(),
  model: 'machines',
});

const receiveMachines = (data) => ({
  ...receiveAllData(data),
  model: 'machines',
});

const fetchMachine = (id) => ({
  ...fetchData(id),
  model: 'machines',
});

const receiveMachine = (data) => ({
  ...receiveData(data),
  model: 'machines',
});

export {fetchMachines, receiveMachines, fetchMachine, receiveMachine};
