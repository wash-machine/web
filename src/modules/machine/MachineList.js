import React, {Component} from 'react';
import MachineListItem from './MachineListItem';
import hocList from '../../lib/crud/hocList';
import {MachineAPI} from './ResourceAPI';
import {connect} from 'react-redux';

const ListComponent = hocList(MachineListItem);

class MachineList extends Component {
  componentWillMount = () => {
    this.setState({fetchInterval: setInterval(this.fetchMachines, 10000)});
  }

  componentWillUnmount = () => {
    clearInterval(this.state.fetchInterval);
  }

  fetchMachines = () => {
    MachineAPI.loadMachines(this.props.dispatch);
  }

  render = () => <ListComponent {...this.props} />

}

export default connect()(MachineList);
