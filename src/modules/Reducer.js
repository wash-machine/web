import {
  FETCH_ALL_DATA, FETCH_DATA, FETCH_TYPES, FETCH_USERS,
  RECEIVE_ALL_DATA, RECEIVE_DATA, RECEIVE_TYPES, /*RECEIVE_USERS,*/
  RECEIVE_ERROR
} from './Actions';

import {
  FETCH_STATES, RECEIVE_STATES
} from './user/Actions';

const updateList = (actualList, itemUpdate) => {
  const newList = actualList.map( (item) => {
    // console.debug('map update list', item);
    if (item.id === itemUpdate.id) {
      return itemUpdate;
    }
    return item;
  });
  if (!newList.length) {
    newList.push(itemUpdate);
  }
  // console.debug('list updated', newList);
  return newList;
};

const initialState = {
  fetching: false, hasTypes: false,
  machines: [], shoe: [], state: [], clients: [], employees: [],
  wash: [], serviceOrders: [], material: [], payment: [],
  addressState: [],
};

const appReducer = (state = initialState, action) => {

  console.info('reducer action:', action.type, 'to model', action.model);
  // console.debug('data', action);
  let models = [];
  switch(action.type) {
    case FETCH_ALL_DATA:
      return {...state, fetching: action.fetching, success: action.success};
    case FETCH_DATA:
      return {...state, fetching: action.fetching, success: action.success};
    case FETCH_STATES:
      return {...state, success: action.success};
    case RECEIVE_STATES:
      return {...state, addressState: action.data};
    case RECEIVE_ALL_DATA:
      return {...state, [action.model]: action.data, fetching: action.fetching};
    case RECEIVE_ERROR:
      return {...state,
        [action.modelError]: action.error, success: action.success};
    case RECEIVE_DATA:
      models = updateList(state[action.model], action.data);
      return {...state,
        [action.model]: models, [action.modelError]: {},
        fetching: action.fetching, success: action.success};
    case FETCH_TYPES:
      return {...state, success: action.success};
    case FETCH_USERS:
      return {...state, success: action.success};
    case RECEIVE_TYPES:
      return {...state,
        wash: action.wash, shoe: action.shoe, state: action.state,
        material: action.material, payment: action.payment,};
    default:
      return state;
  }

};

export default appReducer;
