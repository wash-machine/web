'use-strict';
import { signin_user, signin, signout, login, signinError } from './Actions';
import { request, requestForm } from '../../lib/request';


const AuthAPI = {
  getCurrentUser: (dispatch) => {
    request(
      'https://shoe-washer.herokuapp.com/api/rest-auth/user/',
      'get',
      '',
    ).then( data => dispatch(signin_user(data)));
  },
  signin: (dispatch, data) => {
    dispatch(login());
    requestForm(
      'https://shoe-washer.herokuapp.com/api/rest-auth/login/',
      data,
    ).then( data => {
      dispatch(signin(data));
      AuthAPI.getCurrentUser(dispatch);
    })
    .catch( error => {
      dispatch(signinError(error));
    });
  },
  signout: (dispatch) => {
    return new Promise( (resolve, reject) => {
      resolve();
    }).then( data => dispatch(signout()));
  }
};

export {AuthAPI};
