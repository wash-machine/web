import Token from '../../lib/request/AuthToken';

import {
  LOGIN, SIGNOUT, SIGNIN_USER, SIGNIN, SIGNIN_ERROR,
} from './Actions';

const initialState = {
  user: {},
};

const authReducer = (state=initialState, action) => {
  switch(action.type) {
    case LOGIN:
      return {...state, fetching: action.fetching};
    case SIGNIN:
        Token.saveToken(action.token);
        return {
          ...state,
          fetching: action.fetching,
          isAuthenticated: action.isAuthenticated,
        };
    case SIGNIN_USER:
      if (action.user.is_superuser) {
        return {
          ...state,
          user: action.user,
          isAuthenticated: action.isAuthenticated,
        };
      } else {
        return {
          ...state,
          fetching: action.fetching,
          message: 'Login fail',
          isAuthenticated: action.isAuthenticated,
        };
      }
    case SIGNIN_ERROR:
      return {...state,
        user: action.user,
        token: action.token,
        isAuthenticated: action.isAuthenticated,
        error: action.loginError,
      }
    case SIGNOUT:
      Token.removeToken();
      return {
        ...state,
        user: {},
        isAuthenticated: false,
      };
    default:
      return state;
  }
};

export default authReducer;
