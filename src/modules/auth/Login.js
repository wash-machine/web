import React, { Component } from 'react';
import Formsy from 'formsy-react';
import { AuthAPI } from './ResourceAPI';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import FormsyText from '../../lib/formsy/FormsyText';
import Button from '@material-ui/core/Button';
import Logo from '../../lib/basic/Logo';
import Typography from '@material-ui/core/Typography';

const styles  = {
  form: {
    width: '100%',
  },
  field: {
    marginBottom: '30px',
  },
};

class Login extends Component {

  componentWillMount = () => {
    const { state } = this.props.location;
    if (state && state.from) {
      this.setState({ from: state.from });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    const error = nextProps.error || {};
    if (error.non_field_errors) {
      this.setState({error: error.non_field_errors.join(", ")});
    } else {
      this.setState({error: undefined})
      this.form.updateInputsWithError(error);
    }
  }

  state = {
    from: '/',
    error: undefined,
  }

  login = (data) => {
    AuthAPI.signin(this.props.dispatch, data);
  }

  render = () => {
    const { classes, auth } = this.props;
    if (!auth) {
      return (
        <Grid container spacing={16} align="center" >
          <Formsy
            className={ classes.form }
            onValidSubmit={this.login}
            ref={ (form) => this.form = form }
          >
            <Grid className={ classes.field } item xs={4} >
              <Logo />
            </Grid>
            <Grid className={ classes.field } item xs={4} >
              <FormsyText label={ 'e-mail' } fullWidth name='email' />
            </Grid>
            <Grid className={ classes.field } item xs={4} >
              <FormsyText label={ 'password' } fullWidth type='password' name='password' />
            </Grid>
            <Grid className={ classes.field } item xs={6} >
              <Typography variant='body1' align='center' color='error' >
                {this.state.error}
              </Typography>
            </Grid>
            <Grid className={ classes.field } item xs={4} >
              <Button fullWidth variant='raised' type='submit' color='primary' >
                LOGIN
              </Button>
            </Grid>
          </Formsy>
        </Grid>
      );
    } else {
      return (
        <Redirect to={this.state.from} />
      );
    }
  }
}

export default connect( state => ({
  auth: state.authReducer.isAuthenticated,
  error: state.authReducer.error,
}))(withStyles(styles)(Login));
