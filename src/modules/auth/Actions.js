export const SIGNIN = 'SIGNIN_TOKEN';
export const SIGNIN_USER = 'SIGNIN_USER';
export const LOGIN = 'LOGIN_USER';
export const SIGNOUT = 'SIGNOUT_USER';
export const SIGNIN_ERROR = 'SIGNIN_USER_ERROR';

const login = () => ({
  type: LOGIN,
  fetching: true,
});

const signin_user = (data) => ({
  type: SIGNIN_USER,
  user: data,
  isAuthenticated: true,
});

const signin = (data) => ({
  type: SIGNIN,
  fetching: false,
  token: data.key,
  isAuthenticated: true,
});

const signout = () => ({
  type: SIGNOUT,
  token: undefined,
  user: undefined,
  isAuthenticated: false,
});

const signinError = (error) => ({
  type: SIGNIN_ERROR,
  token: undefined,
  user: undefined,
  isAuthenticated: false,
  loginError: error,
});

export { signin, signin_user, signout, login, signinError };
