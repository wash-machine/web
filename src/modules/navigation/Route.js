import React, { Component } from 'react';
import AuthenticatedRoute from '../../lib/auth/AuthenticatedRoute';

import {
  Route,
  Switch,
} from 'react-router-dom'
import Login from '../auth/Login';

import Loadable from 'react-loadable';
const loading = () => <div>Loading</div>;

const HomeLoader = Loadable({
  loader: () => import('../Home'),
  loading: loading
});

const MachineFormLoader = Loadable({
  loader: () => import('../machine/MachineForm'),
  loading: loading
});

const MachineEditLoader = Loadable({
  loader: () => import('../machine/MachineEdit'),
  loading: loading
});

const MachineLoader = Loadable({
  loader: () => import('../machine/Machine'),
  loading: loading
});

const UserLoader = Loadable({
  loader: () => import('../user/User'),
  loading: loading
});

const UserFormLoader = Loadable({
  loader: () => import('../user/UserForm'),
  loading: loading
});

const ClientEditLoader = Loadable({
  loader: () => import('../user/ClientEdit'),
  loading: loading
});

const EmployeeEditLoader = Loadable({
  loader: () => import('../user/EmployeeEdit'),
  loading: loading
});

const ServiceOrderLoader = Loadable({
  loader: () => import('../service_order/ServiceOrder'),
  loading: loading
});

const ServiceOrderFormLoader = Loadable({
  loader: () => import('../service_order/ServiceOrderForm'),
  loading: loading,
});

const ServiceOrderEditLoader = Loadable({
  loader: () => import('../service_order/ServiceOrderEdit'),
  loading: loading,
});

class AppRoute extends Component {
  render = () => {
    const { has_token } = this.props;
    return (
      <Switch>
        <Route exact path='/login' component={Login} />
        <AuthenticatedRoute has_token={ has_token } exact path='/' component={HomeLoader} />
        <AuthenticatedRoute has_token={ has_token } exact path='/machines' component={MachineLoader} />
        <AuthenticatedRoute has_token={ has_token } path='/machines/new' component={MachineFormLoader} />
        <AuthenticatedRoute has_token={ has_token } path='/machines/edit/:id' component={MachineEditLoader} />
        <AuthenticatedRoute has_token={ has_token } exact path='/users/clients/new' render={
          (props) => <UserFormLoader {...props} />
        } />
        <AuthenticatedRoute has_token={ has_token } exact path='/users/clients/edit/:id' render={
          (props) => <ClientEditLoader {...props} />
        } />
        <AuthenticatedRoute has_token={ has_token } exact path='/users/employeers/new' render={
          (props) => <UserFormLoader {...props} isAdmin />
        } />
        <AuthenticatedRoute has_token={ has_token } exact path='/users/employeers/edit/:id' render={
          (props) => <EmployeeEditLoader {...props} isAdmin />
        } />
        <AuthenticatedRoute has_token={ has_token } path='/users/clients' render={ (props) => <UserLoader {...props} name='clients' /> } />
        <AuthenticatedRoute has_token={ has_token } path='/users/employeers' render={ (props) => <UserLoader {...props} isAdmin name='employeers' /> } />
        <AuthenticatedRoute has_token={ has_token } exact path='/service-orders' component={ServiceOrderLoader} />
        <AuthenticatedRoute has_token={ has_token } path='/service-orders/new' component={ServiceOrderFormLoader} />
        <AuthenticatedRoute has_token={ has_token } path='/service-orders/edit/:id' component={ServiceOrderEditLoader} />
        <AuthenticatedRoute path='/' render={ () => <div> 404 </div> } />
      </Switch>
    );
  }
}

export default AppRoute;
