import React, {Component} from 'react';
import Drawer from '@material-ui/core/Drawer';
import {withStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import PriorityQueue from '../service_order/PriorityQueue';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const styles = {
  title: {
    marginLeft: '10px',
    marginRight: '10px',
    color: '#4a4a4a',
  }
};

class DrawerQueue extends Component {

  state = {
    openDrawer: false,
  }

  handleDrawer = (event) => {
    this.setState((prevState) => ({
      openDrawer: !prevState.openDrawer,
    }));
  }

  render = () => {
    return (
      <div>
        <Drawer
          anchor={'right'}
          open={this.state.openDrawer}
        >
          <IconButton variant='fab' color="primary" onClick={this.handleDrawer} >
            <Icon>send</Icon>
          </IconButton>
          <Typography className={this.props.classes.title} variant='headline' >Queue Service Order</Typography>
          {this.state.openDrawer && <PriorityQueue />}
        </Drawer>
        <Tab
          label={'Queue'}
          onClick={this.handleDrawer}
        />
      </div>
    );
  }

}

export default withStyles(styles)(DrawerQueue);
