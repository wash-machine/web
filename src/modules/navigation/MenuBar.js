import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { AuthAPI } from '../auth/ResourceAPI';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { withRouter, matchPath } from 'react-router'
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import DrawerQueue from './DrawerQueue';

const styles = {
  menuBar: {
    position: 'absolute',
  },
  loginIcon: {
    marginRight: '0px',
  },
  grow: {
    flex: '1 1 auto',
  },
};

class MenuBar extends Component {

  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    auth: PropTypes.bool,
    has_permission: PropTypes.bool
  }

  state = {
    value: '/',
    anchorEl: null,
  }

  routes = [
    {name: 'Home', path: '/', exact: true},
    {name: 'Machine', path: '/machines'},
    {name: 'Client', path: '/users/clients'},
    {name: 'Employee', path: '/users/employeers'},
    {name: 'Service Order', path: '/service-orders'},
  ]

  componentDidMount = () => {
    const r = _.find(this.routes,
      (route) => {
        return matchPath(this.props.history.location.pathname, {
          path: route.path,
          exact: route.exact
        });
      }) || {};

    //console.debug('match route', r);
    this.setState({value: r.path});
  }

  changeValue = (event, value) => {
    this.props.history.push(value);
    this.setState({value});
  }

  handleMenu = (event) => {
    this.setState({anchorEl: event.currentTarget});
  }

  logout = () => {
    AuthAPI.signout(this.props.dispatch);
    this.handleCloseMenu();
  }

  handleCloseMenu = () => {
    this.setState({anchorEl: null});
  }

  render = () => {
    const { auth, has_permission, classes } = this.props;
    if (auth) {
      return (
        <AppBar className={ classes.menuBar }>
          <Toolbar>
            {has_permission &&
              <Tabs
                value={this.state.value}
                onChange={this.changeValue}
              >
                {this.routes.map(
                  (data, idx) =>
                    <Tab key={idx} label={data.name} value={data.path} />
                )}
              </Tabs>
            }
            {has_permission && <DrawerQueue /> }
            <div className={ classes.grow } />
            <div>
              <IconButton
                className={ classes.loginIcon }
                onClick={ this.handleMenu }
                color="inherit"
              >
                <Icon>
                  account_circle
                </Icon>
              </IconButton>
              <Menu
                open={ Boolean(this.state.anchorEl) }
                onClose={ this.handleCloseMenu }
                anchorEl={ this.state.anchorEl }
              >
                <MenuItem onClick={ this.logout } >Logout</MenuItem>
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
      );
    } else {
      return null;
    }
  }
}

// TODO: remove withrouter and use redux router state
const getState = (state) => {
  const user = state.authReducer.user || {};
  return {
    auth: state.authReducer.isAuthenticated,
    has_permission: user.is_superuser,
  };
}
export default connect(getState)(withRouter(withStyles(styles)(MenuBar)));
