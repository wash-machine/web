'use-strict';
import {receiveTypes, fetchTypes, fetchUsers} from './Actions';
import { request } from '../lib/request';
import { receiveClients } from './user/Actions';

const fakeData = {
  users: [
    {
      name: 'Marcelo',
      id: 1,
    }, {
      name: 'Rayanne',
      id: 2,
    }, {
      name: 'Carla',
      id: 3,
    }
  ]
};

const AppAPI = {
  loadTypes: (dispatch) => {
    dispatch(fetchTypes()); // Notify get

    // Create gets
    const washType = request('https://shoe-washer.herokuapp.com/api/service_order/wash_type/');
    const shoesType = request('https://shoe-washer.herokuapp.com/api/service_order/shoe_type/');
    const stateType = request('https://shoe-washer.herokuapp.com/api/service_order/service_order_state/');
    const materialType = request('https://shoe-washer.herokuapp.com/api/service_order/material/');
    const paymentType = request('https://shoe-washer.herokuapp.com/api/service_order/payment_type/');

    // Perform get
    Promise.all([washType, shoesType, stateType, materialType, paymentType])
      .then( data => {
        console.log(data);
        const wash = data[0];
        const shoe = data[1];
        const state = data[2];
        const material = data[3];
        const payment = data[4];
        dispatch(receiveTypes({ wash, shoe, state, material, payment }));
      });
  },
  loadUsers: (dispatch) => {
    dispatch(fetchUsers());
    Promise.resolve(fakeData.users)
      .then( data => dispatch(receiveClients(data)));
  },
};

export {AppAPI};
