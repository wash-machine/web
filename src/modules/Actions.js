export const FETCH_ALL_DATA = 'LOAD_ALL_MODEL_DATA';
export const FETCH_DATA = 'LOAD_ONE_DATA';
export const FETCH_TYPES = 'FETCH_TYPES';
export const FETCH_USERS = 'FETCH_USERS';
export const RECEIVE_ALL_DATA = 'RECEIVE_ALL_DATA';
export const RECEIVE_DATA = 'RECEIVE_ONE_DATA';
export const RECEIVE_TYPES = 'RECEIVE_TYPES';
export const RECEIVE_ERROR = 'RECEIVE_ERROR';

const fetchAllData = () => ({
  type: FETCH_ALL_DATA,
  fetching: true,
  success: undefined,
});

const receiveAllData = (data) => ({
  type: RECEIVE_ALL_DATA,
  fetching: false,
  data: data
});

const fetchData = () => ({
  type: FETCH_DATA,
  fetching: true,
  success: undefined,
});

const receiveData = (data) => ({
  type: RECEIVE_DATA,
  fetching: false,
  success: data.id,
  data: data,
});

const fetchTypes = () => ({
  type: FETCH_TYPES,
  hasTypes: false,
  wash: [],
  shoe: [],
  state: [],
  success: undefined,
});

const receiveTypes = (data) => ({
  type: RECEIVE_TYPES,
  hasTypes: true,
  wash: data.wash,
  shoe: data.shoe,
  state: data.state,
  material: data.material,
  payment: data.payment,
});

const receiveError = (data) => ({
  type: RECEIVE_ERROR,
  fetching: false,
  success: undefined,
  error: data,
});

// TODO: REMOVE
const fetchUsers = () => ({})

export {
  fetchAllData, fetchData, fetchTypes,
  receiveAllData, receiveData, receiveTypes,
  receiveError, fetchUsers
};
