import React, { Component } from 'react';

import { connect } from 'react-redux';
//import { matchPath } from 'react-router';
import { UserAPI } from './ResourceAPI';
import { withStyles } from '@material-ui/core/styles';
//import Tabs, { Tab } from 'material-ui/Tabs';
//import LoadingBase from '../../lib/load/LoadingBase';
import Grid from '@material-ui/core/Grid';
import UserList from './UserList';
import TitleNew from '../../lib/basic/TitleNew';
//import UserRoute from '../navigation/UserRoute';

const styles = {
  subNavigation: {
    marginBottom: "20px",
  }
};

class User extends Component {

  componentDidMount = () => {
    UserAPI.loadClients(this.props.dispatch);
    UserAPI.loadEmployees(this.props.dispatch);
  }

  static defaultProps = {
    isAdmin: false,
  }

  filterAdmin = (user) => {
    //console.debug('user', user.is_superuser, '==', this.props.isAdmin);
    return user.is_superuser === this.props.isAdmin;
  }

  filterId = (user) => {
    const { location } = this.props;
    if (location && location.search) {
      const idParam = location.search.match(/id=(\d+)/);
      //console.debug('user', user.id, '==', idParam);
      if (idParam.length) {
        const id = idParam[1];
        return Number(user.id) === Number(id);
      }
      return false;
    }
    return true;
  }

  filterUsers = () => {
    if (this.props.isAdmin) {
      return this.props.employees.filter(this.filterId);
    } else {
      return this.props.clients.filter(this.filterId);
    }
  }

  render = () => {
    const { fetching } = this.props;
    const data = this.filterUsers();
    //console.debug('User data', data);
    return (
      <Grid container spacing={0} >
          <TitleNew title='Users' newLink={`/users/${this.props.name}/new`} />
          <Grid item xs={12} >
            <UserList fetching={ fetching } name={this.props.name} dataList={ data } />
          </Grid>
      </Grid>
    );
  }
}

const StyledUser = withStyles(styles)(User);
export default connect(state => ({
  clients: state.appReducer.clients,
  employees: state.appReducer.employees,
  fetching: state.appReducer.fetching
}))(StyledUser);
