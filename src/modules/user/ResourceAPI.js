'use-strict';
import {
  receiveClients, receiveEmployees, fetchUsers, receiveEmployee,
  receiveClient, fetchUser, fetchStates, receiveStates,
  receiveClientError, receiveEmployeeError,
} from './Actions';
import { request, requestForm } from '../../lib/request';


const UserAPI = {
  loadClients: (dispatch) => {
    dispatch(fetchUsers()); // Notify get

    // Perform get
    return request('https://shoe-washer.herokuapp.com/api/user_manager/client/')
      .then( data => dispatch(receiveClients(data)));
  },
  loadEmployees: (dispatch) => {
    dispatch(fetchUsers()); // Notify get

    // Perform get
    return request('https://shoe-washer.herokuapp.com/api/user_manager/employee/')
      .then( data => dispatch(receiveEmployees(data)));
  },
  loadClient: (dispatch, id) => {
    dispatch(fetchUser());
    return request(`https://shoe-washer.herokuapp.com/api/user_manager/client/${id}/`)
      .then( data => dispatch(receiveClient(data)));
  },
  loadEmployee: (dispatch, id) => {
    dispatch(fetchUser());
    return request(`https://shoe-washer.herokuapp.com/api/user_manager/employee/${id}/`)
      .then( data => dispatch(receiveEmployee(data)));
  },
  loadStates: (dispatch) => {
    dispatch(fetchStates());
    return request('https://shoe-washer.herokuapp.com/api/user_manager/state/')
      .then( data => dispatch(receiveStates(data)));
  },
  saveClient: (dispatch, data) => {
    const residential = {
      'address': data.address,
      'city': data.city,
      'neighborhood': data.neighborhood,
      'cep': data.cep,
      'number': data.number,
      'complement': data.complement,
      'state_id': data.state_id,
    };

    // Nested Form
    requestForm('https://shoe-washer.herokuapp.com/api/user_manager/residential_data/', 
      residential)
      .then( (address) => {
        data['residential_data_id'] = address.id;
        requestForm('https://shoe-washer.herokuapp.com/api/user_manager/client/', data)
          .then( (user) => {
            dispatch(receiveClient(user, user.id));
          })
          .catch( (error) => {
            dispatch(receiveClientError(error));
          });
      }).catch( (error) => {
        dispatch(receiveClientError(error));
      });
  },
  saveEmployee: (dispatch, data) => {
    requestForm('https://shoe-washer.herokuapp.com/api/user_manager/employee/', data)
      .then( (user) => {
          dispatch(receiveEmployee(user, user.id));
      })
      .catch( (error) => {
        dispatch(receiveEmployeeError(error));
      });
  },
  saveUser: (dispatch, data) => {
    dispatch(fetchUser());
    if (data.is_superuser) {
      UserAPI.saveEmployee(dispatch, data);
    } else {
      UserAPI.saveClient(dispatch, data);
    }
  }
};

export {UserAPI};
