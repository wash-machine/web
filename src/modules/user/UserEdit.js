import React, { Component } from 'react';
import PropTypes from 'prop-types';
import UserForm from './UserForm';

class UserEdit extends Component {

  static defaultProps = {
    user: {}
  }

  static propTypes = {
    user: PropTypes.object,
  }

  render = () => {
    const title = this.props.isAdmin ? 'Edit Employee':'Edit Client';
    return (
      <UserForm
        title={title}
        item={this.props.user}
        isAdmin={this.props.isAdmin}
      />
    );
  }
}

export default UserEdit;
