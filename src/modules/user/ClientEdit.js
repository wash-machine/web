import UserEdit from './UserEdit';
import {UserAPI} from './ResourceAPI';
import {connect} from 'react-redux';
import hocEdit from '../../lib/crud/hocEdit';

export default connect( state => ({
  items: state.appReducer.clients,
  fetching: state.appReducer.fetching,
}))(hocEdit(UserEdit, UserAPI.loadClient, 'user'));
