import React, { Component } from 'react';
import PropTypes from 'prop-types';
import hocList from '../../lib/crud/hocList';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
// import Opacity from '@material-ui/icons/Opacity';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Collapse from '@material-ui/core/Collapse';

const formatPhone = (phone) => phone.replace(/[^0-9]/g,'')
  .replace(/(\d{2})(\d{4,5})(\d{4})/g, '($1) $2-$3')
const formatCPF = (cpf) => cpf.replace(/[^0-9]/g,'')
  .replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4')

const styles = {
  item: {
    paddingLeft: 0,
    paddingRight: 0
  },
};

class UserListItem extends Component {

  static propTypes = {
    item: PropTypes.object,
  }

  state = {
    openAddress: false,
    openPessoal: false,
  }

  formatCity = (item) => {
    const address = item.residential_data || {state: {}};
    return (
      <Collapse in={this.state.openAddress} unmountOnExit >
        <List>
          <ListItem key={0}>
            <Grid container>
              <Grid item xs={4}>CEP:</Grid>
              <Grid item xs={8}>{address.cep}</Grid>
            </Grid>
          </ListItem>
          <ListItem key={1}>
            <Grid container>
              <Grid item xs={4}>Address:</Grid>
              <Grid item xs={8}>{address.address}</Grid>
            </Grid>
          </ListItem>
          <ListItem key={2}>
            <Grid container>
              <Grid item xs={4}>Number:</Grid>
              <Grid item xs={8}>{address.number}</Grid>
            </Grid>
          </ListItem>
          <ListItem key={3}>
            <Grid container>
              <Grid item xs={4}>City:</Grid>
              <Grid item xs={8}>{address.city}</Grid>
            </Grid>
          </ListItem>
          <ListItem divider key={4}>
            <Grid container>
              <Grid item xs={4}>State:</Grid>
              <Grid item xs={8}>{address.state.abbreviation}</Grid>
            </Grid>
          </ListItem>
        </List>
      </Collapse>
    );
  }

  formatPessoal = (item) => (
    <Collapse in={this.state.openPessoal} unmountOnExit >
      <List>
        <ListItem key={0}>
          <Grid container>
            <Grid item xs={4}>Name:</Grid>
            <Grid item xs={8}>{item.name}</Grid>
          </Grid>
        </ListItem>
        <ListItem divider key={1}>
          <Grid container>
            <Grid item xs={4}>CPF:</Grid>
            <Grid item xs={8}>{formatCPF(item.cpf)}</Grid>
          </Grid>
        </ListItem>
      </List>
    </Collapse>
  )


  handleOpen = (name) => this.setState(
    (prevState) => ({...prevState, [name]: !prevState[name] })
  )

  render = () => {
    const {item } = this.props;
    return (
      <Card>
        <CardHeader
          title={item.name}
          subheader={item.telephone || ''}
        />
        <CardContent>
          <List>
            <ListItem divider>
              <Grid container>
                <Grid item xs={4}>Email:</Grid>
                <Grid item xs={8}>{item.email} </Grid>
              </Grid>
            </ListItem>
            {!item.is_superuser &&
              <ListItem button onClick={() => this.handleOpen('openAddress')} >
                <ListItemText inset primary="Address" />
                {this.state.openAddress ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
            }
            {!item.is_superuser && this.formatCity(item)}
            <ListItem button onClick={() => this.handleOpen('openPessoal')} >
              <ListItemText inset primary="Personal Data" />
              {this.state.openPessoal ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            {this.formatPessoal(item)}
          </List>

        </CardContent>
        <CardActions>
          <Button color='primary'
            component={ (props) =>
              <Link to={`/users/${this.props.name}/edit/${item.id}/`} {...props} />
            }
          >
            Edit
          </Button>
        </CardActions>
      </Card>
    );
  }
}

export default hocList(withStyles(styles)(UserListItem));
