import React, { Component } from 'react';
import CardForm from '../../lib/crud/CardForm';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { UserAPI }  from './ResourceAPI';
import Grid from '@material-ui/core/Grid';
import GridTextField from '../../lib/formsy/GridTextField';
import GridSelectField from '../../lib/formsy/GridSelectField';
import FormsyHidden from '../../lib/formsy/FormsyHidden';
import MenuItem from '@material-ui/core/MenuItem';

class UserForm extends Component {

  static propTypes = {
    item: PropTypes.object,
    title: PropTypes.string,
  }

  static defaultProps = {
    item: {address: {}},
  }

  componentWillMount = () => {
    UserAPI.loadStates(this.props.dispatch);
  }

  mapStates = (states) => states.sort( (a, b) => a.name > b.name)
    .map( (state, idx) =>
      <MenuItem key={idx} value={state.id} >{state.name}</MenuItem>
    )

  getFields = () => {
    const fields = {
      first_name: {
        label: 'Name',
        name: 'first_name',
        placeholder: 'Name',
        helperText: 'Insert here the first name',
      },
      last_name: {
        label: 'Surname',
        name: 'last_name',
        placeholder: 'name',
        helperText: 'Insert here the last name',
      },
      email: {
        label: 'E-mail',
        name: 'email',
        placeholder: 'example@domain.ex',
        helperText: 'This e-mail will be used to send infos',
      },
      cpf: {
        label: 'CPF',
        name: 'cpf',
        placeholder: '00000000000',
      },
      telephone: {
        label: 'Phone Number',
        name: 'telephone',
        placeholder: '61 9 0000-0000',
        helperText: 'Contact residental or mobile phone number',
      },
      address_id: {
        label: 'Address',
        name: 'residential_data_id',
      },
      address: {
        label: 'Address',
        name: 'address',
      },
      city: {
        label: 'City',
        name: 'city',
      },
      neighborhood: {
        label: 'Neighborhood',
        name: 'neighborhood',
      },
      cep: {
        label: 'Cep',
        name: 'cep',
      },
      number: {
        label: 'Number',
        name: 'number',
      },
      complement: {
        label: 'Complement',
        name: 'complement',
      },
      state_id: {
        label: 'State',
        name: 'state_id',
      },
    };
    return fields;
  }

  onSubmit = (data) => {
    UserAPI.saveUser(this.props.dispatch, data);
  }

  getAddressFields = (fields, address={}) => {
    return [
      <FormsyHidden name={'address_id'} value={address.id} />,
      <GridTextField field={fields.address} value={address.address} xs={8} />,
      <GridTextField field={fields.number} value={address.number} xs={1} />,
      <GridTextField field={fields.cep} value={address.cep} xs={3} />,
      <GridTextField field={fields.complement} value={address.complement} xs={4} />,
      <GridTextField field={fields.neighborhood} value={address.neighborhood} xs={4}/>,
      <GridTextField field={fields.city} value={address.city} xs={2} />,
      <GridSelectField field={fields.state_id} value={address.state_id}
        options={this.mapStates(this.props.states)} xs={2} />
    ];
  }

  render = () => {
    const {isAdmin, item} = this.props;
    const successUrl = isAdmin ? 'employeers':'clients';
    const error = isAdmin ? this.props.employeeError : this.props.clientError;
    const fields = this.getFields();
    const user = ['first_name', 'last_name', 'email', 'cpf', 'telephone', ];
    return (
      <CardForm
        title={this.props.title || 'New user'}
        error={error}
        onSubmit={this.onSubmit}
        successUrl={`/users/${successUrl}/`}
        success={this.props.success}
      >
        <Grid container spacing={40} >
          <FormsyHidden name={"is_superuser"} value={Boolean(isAdmin)} />
          <FormsyHidden name={"id"} value={item.id} />
          {user.map(
            (name, idx) => {
              return <GridTextField
                key={idx} field={fields[name]} value={item[name]}
              />
            }
          )}

          {!isAdmin && this.getAddressFields(fields, item.residential_data || {})}

        </Grid>
      </CardForm>
    );
  }
}

export default connect( (state) => ({
  states: state.appReducer.addressState,
  success: state.appReducer.success,
  clientError: state.appReducer.clientError,
  employeeError: state.appReducer.employeeError,
}))(UserForm);
