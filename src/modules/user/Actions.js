import {fetchData, receiveData, fetchAllData, receiveAllData, receiveError} from '../Actions';

export const FETCH_STATES = 'FETCH_STATES';
export const RECEIVE_STATES = 'RECEIVE_STATES';

const fetchUsers = () => ({
  ...fetchAllData(),
  model: 'users',
});

const receiveClients = (data) => ({
  ...receiveAllData(data),
  model: 'clients',
});

const receiveEmployees = (data) => ({
  ...receiveAllData(data),
  model: 'employees',
});

const fetchUser = (id) => ({
  ...fetchData(id),
  model: 'users',
});

const receiveClient = (data, success=undefined) => ({
  ...receiveData(data),
  model: 'clients',
  success: success,
});

const receiveClientError = (error) => ({
  ...receiveError(),
  modelError: 'clientError',
  error: error
});

const receiveEmployee = (data, success=undefined) => ({
  ...receiveData(data),
  model: 'employees',
  success: success,
});

const receiveEmployeeError = (error) => ({
  ...receiveError(),
  modelError: 'employeeError',
  error: error
});

const fetchStates = () => ({
  type: FETCH_STATES,
  success: undefined,
});

const receiveStates = (states) => ({
  type: RECEIVE_STATES,
  data: states,
});

export {
  fetchUsers, receiveClients, receiveEmployees, fetchStates,
  fetchUser, receiveClient, receiveClientError, receiveEmployee,
  receiveEmployeeError, receiveStates,
};
