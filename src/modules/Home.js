import React, { Component } from 'react';
import { AppAPI } from './ResourceAPI';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';

import TypeList from './TypeList';

class Home extends Component {

  componentWillMount = () => {
    if (!this.props.hasTypes){
      AppAPI.loadTypes(this.props.dispatch);
    }
  }

  static defaultProps = { material: [], wash: [], shoe: [], state: [] }

  render = () => {
    const { wash, shoe, state, material } = this.props;
    const typesInfo = [
      { types: wash, title: 'Wash', name: 'wash' },
      { types: shoe, title: 'Shoe', name: 'shoe' },
      { types: state, title: 'State', name: 'state' },
      { types: material, title: 'Material', name: 'material' },
    ];

    return (
      <div>
        <Grid container spacing={24} >
          <Grid item xs={12} >
            <h2>Use informations</h2>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
          </Grid>
          <Grid item xs={12}>
            <h2>Types in platform</h2>
          </Grid>
          {
            typesInfo.map( (types, key) =>
                  <TypeList key={ key } location={this.props.location} { ...types } />
          )
        }
        </Grid>
      </div>
    );
  }
}


export default connect( state => ({
  wash: state.appReducer.wash,
  material: state.appReducer.material,
  shoe: state.appReducer.shoe,
  state: state.appReducer.state,
  hasTypes: state.appReducer.hasTypes,
}))(Home);
