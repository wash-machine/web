'use-strict';
import {
  receiveServiceOrders, fetchServiceOrders,
  receiveServiceOrder, fetchServiceOrder,
  receiveErrorServiceOrders,
  receivePriorityQueue, fetchPriorityQueue
} from './Actions';
import { request, requestForm } from '../../lib/request';


const ServiceOrderAPI = {
  loadServiceOrders: (dispatch) => {
    dispatch(fetchServiceOrders()); // Notify get
    // Perform get
    request('https://shoe-washer.herokuapp.com/api/service_order/service_order/')
      .then( data => dispatch(receiveServiceOrders(data)));
  },
  loadServiceOrder: (dispatch, id) => {
    dispatch(fetchServiceOrder());
    return request(`https://shoe-washer.herokuapp.com/api/service_order/service_order/${id}/`)
      .then( data => dispatch(receiveServiceOrder(data)));
  },
  saveServiceOrder: (dispatch, data) => {
    // Nested Form
    dispatch(fetchServiceOrder());
    const shoeData = {id: data.shoe_id,
      material: data.material, shoe_type: data.shoe_type};
    requestForm('https://shoe-washer.herokuapp.com/api/service_order/shoe/',
                shoeData)
      .then( (shoe) => {
        data.shoe_id = shoe.id;
        // Main Form
        return requestForm('https://shoe-washer.herokuapp.com/api/service_order/service_order/',
                           data)
                .catch( (error) => dispatch(receiveErrorServiceOrders(error)) );
      })
      .then( data => dispatch(receiveServiceOrder(data, data.id)))
      .catch( (error) => dispatch(receiveErrorServiceOrders(error)) );
  },
  loadPriorityQueue: (dispatch) => {
    dispatch(fetchPriorityQueue());
    return request('https://shoe-washer.herokuapp.com/api/service_order/service_order_queue/').
      then( (priorityQueue) => {
        dispatch(receivePriorityQueue(priorityQueue));
      });
  }
};

export {ServiceOrderAPI};
