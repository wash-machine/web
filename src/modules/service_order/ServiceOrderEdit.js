import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ServiceOrderForm from './ServiceOrderForm';
import { ServiceOrderAPI } from './ResourceAPI';
import hocEdit from '../../lib/crud/hocEdit';

class ServiceOrderEdit extends Component {

  static defaultProps = {
    serviceOrder: {shoe: {}}
  }

  static propTypes = {
    serviceOrder: PropTypes.object,
  }

  render = () => {
    return (
      <ServiceOrderForm
        title={ 'Edit service order' }
        item={ this.props.serviceOrder }
      />
    );
  }
}

export default connect( state => ({
  items: state.appReducer.serviceOrders,
  fetching: state.appReducer.fetching,
}))(hocEdit(ServiceOrderEdit, ServiceOrderAPI.loadServiceOrder, 'serviceOrder'));
