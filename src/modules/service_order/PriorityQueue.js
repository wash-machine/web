import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ServiceOrderAPI} from './ResourceAPI';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import dateformat from 'dateformat';
import { push } from 'react-router-redux';

class PriorityQueue extends Component {

  static defaultProps = {
    serviceOrderQueue: []
  }

  state = {
    timeout: 0,
  }

  componentWillMount = () => {
    this.fetchPriority(this.props.dispatch);
    this.setState({timeout: setInterval(this.fetchPriority, 4000, this.props.dispatch)});
    console.log('how');
  }

  componentWillUnmount = () => {
    clearInterval(this.state.timeout);
    console.log('you show your ungly face');
  }

  fetchPriority = () => {
    ServiceOrderAPI.loadPriorityQueue(this.props.dispatch);
  }

  redirectTo = (queue) => {
    // console.log(this.props.router);
    console.log(queue);
    this.props.dispatch(push(`/service-orders/?id=${queue.id}`));
  }

  render = () => {
    return (
      <List>
        <Divider />
        {this.props.serviceOrderQueue.map( (queue, index) => {
          return (
            <div key={index} >
              <ListItem button onClick={() => this.redirectTo(queue) } >
                <ListItemText primary={`${index + 1} - ${queue.client_data.first_name}`} secondary={dateformat(queue.deadline, 'HH:MM dd/mm')} />
              </ListItem>
              <Divider />
            </div>
          );
        })}
      </List>
    );
  }
}

export default connect( (state) => ({
  serviceOrderQueue: state.appReducer.serviceOrdersQueue,
  router: state.router,
}))(PriorityQueue);
