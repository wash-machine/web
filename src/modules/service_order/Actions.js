import {fetchData, receiveData, fetchAllData, receiveAllData, receiveError} from '../Actions';

const receiveErrorServiceOrders = (error) => ({
  ...receiveError(error),
  modelError: 'serviceOrdersError',
});

const fetchServiceOrders = () => ({
  ...fetchAllData(),
  model: 'serviceOrders',
});

const receiveServiceOrders = (data) => ({
  ...receiveAllData(data),
  model: 'serviceOrders',
});

const fetchServiceOrder = (id) => ({
  ...fetchData(id),
  model: 'serviceOrders',
});

const receiveServiceOrder = (data, success=undefined) => ({
  ...receiveData(data),
  model: 'serviceOrders',
  modelError: 'serviceOrdersError',
  success: success,
});

const receivePriorityQueue = (data, success=undefined) => ({
  ...receiveAllData(data),
  model: 'serviceOrdersQueue',
  success: success,
});

const fetchPriorityQueue = () => ({
  ...fetchAllData(),
  model: 'serviceOrdersQueue',
  fetching: false,
});

export {
  fetchServiceOrders, receiveServiceOrders, fetchServiceOrder,
  receiveServiceOrder, receiveErrorServiceOrders,
  receivePriorityQueue, fetchPriorityQueue
};
