import React, { Component } from 'react';

import {connect} from 'react-redux';
import {ServiceOrderAPI} from './ResourceAPI';
//import LoadingBase from '../../lib/load/LoadingBase';
import Grid from '@material-ui/core/Grid';
import TitleNew from '../../lib/basic/TitleNew';
import ServiceOrderList from './ServiceOrderList';


class ServiceOrder extends Component {

  componentDidMount = () => {
    ServiceOrderAPI.loadServiceOrders(this.props.dispatch);
  }

  filterId = (serviceOrder) => {
    const { location } = this.props;
    if (location && location.search) {
      const idParam = location.search.match(/id=(\d+)/);
      //console.debug('user', user.id, '==', idParam);
      if (idParam.length) {
        const id = idParam[1];
        return Number(serviceOrder.id) === Number(id);
      }
      return false;
    }
    return true;
  }

  render = () => {
    const { fetching } = this.props;
    const serviceOrders = this.props.serviceOrders.filter(this.filterId);
    //console.debug('ServiceOrder data', serviceOrders);
    return (
      <Grid container spacing={0}>
        <TitleNew title='Service Orders' newLink='/service-orders/new' />
        <Grid item xs={12}>
          <ServiceOrderList fetching={ fetching } dataList={ serviceOrders } />
        </Grid>
      </Grid>
    );
  }
}

export default connect(state => ({
  serviceOrders: state.appReducer.serviceOrders,
  fetching: state.appReducer.fetching
}))(ServiceOrder);
