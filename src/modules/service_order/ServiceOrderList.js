import React, { Component } from 'react';
import PropTypes from 'prop-types';
import hocList from '../../lib/crud/hocList';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
// import Opacity from '@material-ui/icons/Opacity';
import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Divider from '@material-ui/core/Divider';
import { Link } from 'react-router-dom';
import dateformat from 'dateformat';
// import _ from 'lodash';

const styles = {
  item: {
    paddingLeft: 0,
    paddingRight: 0
  },
  expand: {
    transform: 'rotate(180deg)',
  },
  collapse: {
    transform: 'rotate(0deg)',
  }
};

class ServiceOrderListItem extends Component {

  static propTypes = {
    item: PropTypes.object,
  }

  state = {
    basic: false,
  }

  handleOpen = (name) => {
    this.setState( (state) => {
      return {...state, basic: !state.basic};
    });
  }

  getCardAction = (expanded, name) => {
  }

  render = () => {
    const { item } = this.props;
    const links = [
      {
        path: '/',
        name: 'wash',
        label: 'Wash type',
        item: item.wash_type_data.name,
        id: item.wash_type
      }, {
        path: '/',
        label: 'State',
        name: 'state',
        item: item.state_data.name,
        id: item.state
      }
    ];
    const singleValues = [
      { label: 'Value', item: item.payment_value },
      { label: 'Date start', item: dateformat(item.start_time, 'HH:MM dd/mm') },
    ];

    return (
      <Card>
        <CardHeader
          title={`Order ${item.id}`}
          subheader={item.state.name}
        />
        <Divider />
        <CardContent>
          <List>
            <ListItem>
                <Grid item xs={4}>Client: </Grid>
                <Grid item xs={8}>
                  <Link to={{
                    pathname: '/users/clients',
                    search: `?id=${item.client}`,
                  }}
                  >{item.client_data.first_name}</Link>
                </Grid>
            </ListItem>
            <ListItem divider>
                <Grid item xs={5}>Deadline: </Grid>
                <Grid item xs={7}>{dateformat(item.deadline, 'HH:MM dd/mm')}</Grid>
            </ListItem>
            <ListItem button onClick={this.handleOpen} >
              <ListItemText inset primary="Detalhes" />
              {this.state.basic ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={this.state.basic} timeout="auto" unmountOnExit>
                <List>
                  {
                    singleValues.map( (value, key) => {
                      return (
                        <ListItem key={key}>
                          <Grid item xs={4}>{value.label}: </Grid>
                          <Grid item xs={8}>{value.item}</Grid>
                        </ListItem>
                      );
                    })
                  }
                  {
                    links.map((value, key) => {
                      return (
                        <ListItem key={key}>
                            <Grid item xs={4}>{value.label}: </Grid>
                            <Grid item xs={8}>
                              <Link to={{
                                pathname: value.path,
                                search: `?id=${value.id}`,
                                hash: `#${value.name}`,
                              }}
                              >{value.item}</Link>
                            </Grid>
                        </ListItem>);
                    })
                  }
                </List>
            </Collapse>
          </List>
        </CardContent>
        <Divider />
        <CardActions>
          <Button color='primary'
            component={ (props) =>
              <Link to={`/service-orders/edit/${item.id}/`} {...props} />
            }
          >
            Edit
          </Button>
        </CardActions>
      </Card>
    );
  }
}

export default hocList(withStyles(styles)(ServiceOrderListItem));
