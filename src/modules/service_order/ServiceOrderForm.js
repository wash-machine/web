import React, { Component } from 'react';
import CardForm from '../../lib/crud/CardForm';
import GridSelectField from '../../lib/formsy/GridSelectField';
import GridTextField from '../../lib/formsy/GridTextField';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
//import {FormsyTextField} from '../form/formsy-fields';
import Grid from '@material-ui/core/Grid';
import  MenuItem from '@material-ui/core/MenuItem';
import { AppAPI } from '../ResourceAPI';
import { ServiceOrderAPI } from './ResourceAPI';
import { UserAPI } from '../user/ResourceAPI';

class ServiceOrderForm extends Component {

  static propTypes = {
    item: PropTypes.object,
    title: PropTypes.string,
  }

  static defaultProps = {
    wash: [],
    shoe: [],
    user: [],
    state: [],
    material: [],
    item: {shoe: {}},
  }

  componentWillMount = () => {
    AppAPI.loadTypes(this.props.dispatch);
    UserAPI.loadClients(this.props.dispatch);
  }

  mapOptionsField = (options) => {
    
    return options.sort( (a,b) => a.name > b.name).map( (option, key) => {
      console.log(option.id, option.name);
      return (
      <MenuItem key={key} value={option.id}>{option.name}</MenuItem>
    )}
    );
  }

  getFields = () => {
    const fields = {
      id: {
        label: 'Service order',
        name: 'id',
        placeholder: 'Service order serial id',
        helperText: 'Unique serial for Service Order. Ex.: 1111',
        isFormDisabled: () => true,
      },
      value: {
        label: 'Value',
        name: 'payment_value',
        placeholder: '00,00',
        helperText: 'Value for this SO. R$: 00,00',
      },
      payment_type: {
        label: 'Select the payment form',
        name: 'payment_type',
      },
      wash_type: {
        label: 'Wash type',
        name: 'wash_type',
      },
      client: {
        label: 'Client',
        name: 'client',
      },
      state: {
        label: 'State',
        name: 'state',
      },
      deadline: {
        label: 'Date to delivery',
        name: 'deadline',
      },
      wash_end: {
        label: 'Delivered date',
        name: 'wash_end',
      },
      observation: {
        label: 'Addictional Commentary',
        name: 'observation',
        multiline: true,
        placeholder: '',
        helperText: 'Add more commentaries',
      },
      shoe_id: {
        name: 'shoe_id',
        label: 'Shoe',
        isFormDisabled: () => true,
      },
      shoe_type: {
        label: 'Shoes type',
        name: 'shoe_type',
      },
      material: {
        label: 'Material type',
        name: 'material',
      }
    };
    return fields;
  }

  onSubmit = (data) => {
    ServiceOrderAPI.saveServiceOrder(this.props.dispatch, data);
  }

  render = () => {
    const fields = this.getFields();
    const {item} = this.props;
    const shoe = item.shoe || {};
    const { payment, wash, client, state, shoe_type, material } = this.props;
    return (
      <CardForm
        title={this.props.title || 'New Service Order'}
        error={this.props.error}
        onSubmit={this.onSubmit}
        successUrl={'/service-orders/'}
        success={this.props.success}
      >
        <Grid container spacing={40} >
          <GridTextField value={item.id} field={fields.id} />
          <GridTextField value={item.shoe_id} field={fields.shoe_id} />
          <GridSelectField field={fields.shoe_type} value={shoe.shoe_type}
            options={this.mapOptionsField(shoe_type)} />
          <GridSelectField value={shoe.material} field={fields.material}
            options={this.mapOptionsField(material)} />
          <GridSelectField value={item.wash_type} field={fields.wash_type}
            options={this.mapOptionsField(wash)} />
          <GridSelectField value={item.client} field={fields.client}
            options={this.mapOptionsField(client)} />
          <GridTextField value={item.payment_value} field={fields.value} />
          <GridTextField value={item.deadline} field={fields.deadline} />
          <GridSelectField value={item.state} field={fields.state}
            options={this.mapOptionsField(state)} />
          <GridSelectField value={item.payment_type} field={fields.payment_type}
            options={this.mapOptionsField(payment)} />
          <GridTextField value={item.wash_end} field={fields.wash_end} />
          <GridTextField value={item.observation} field={fields.observation}
            xs={12} />
        </Grid>
      </CardForm>
    );
  }
}

export default connect( state => ({
  wash: state.appReducer.wash,
  shoe_type: state.appReducer.shoe,
  state: state.appReducer.state,
  client: state.appReducer.clients,
  material: state.appReducer.material,
  payment: state.appReducer.payment,
  error: state.appReducer.serviceOrdersError,
  success: state.appReducer.success,
}))(ServiceOrderForm);
