import { createMuiTheme } from '@material-ui/core/styles';

import blueGray from '@material-ui/core/colors/blue';
import orange  from '@material-ui/core/colors/orange';
import red from '@material-ui/core/colors/red';

const theme = {
  palette: {
    primary: {
      main: blueGray[900],
    },
    secondary: {
      main: orange[600],
    },
    error: {
      main: red[700],
    }
  }
};

export default createMuiTheme(theme);
